const express = require("express"); // import express
const multer = require("multer"); // for file upload

const PORT = process.env.PORT || 8000; // set port
const Controller = require("./controller/carsController"); // import controller
const app = express(); // create express app
const path = require("path"); // import path
const bodyParser = require("body-parser"); // import body parser
const diskStorage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, path.join(__dirname, "./uploads")); // set destination
    },
    filename: function (req, file, cb) {
        cb(null, file.fieldname + "-" + Date.now() + path.extname(file.originalname)); // set filename
    },
});

require("dotenv").config();
const {HOMEPAGE, CREATE_CAR, EDIT_CAR, FILTER_RESULT, SEARCH_RESULT} = process.env;
const {API_CREATE_CAR, API_GET_CARS, API_GET_CAR, API_SEARCH_CAR, API_FILTER_CAR, API_UPDATE_CAR, API_DELETE_CAR, API_CREATE_TYPE_CAR} = process.env;

app.set("view engine", "ejs"); // set view engine to ejs
app.use(express.json()); // for parsing application/json
app.use(express.static("public")); // for static files
app.use(express.static("uploads")); // for static files
app.use(bodyParser.urlencoded({extended: true})); // for parsing application/x-www-form-urlencoded
app.use(bodyParser.json()); // for parsing application/json

// Client Side
app.get(HOMEPAGE, Controller.homepageView); // homepage
app.get(CREATE_CAR, Controller.inputView); // create car page
app.get(EDIT_CAR, Controller.editView); // edit car page
app.get(FILTER_RESULT, Controller.filterResult); // filter result
app.post(SEARCH_RESULT, Controller.searchResult); // search result
// END Client Side

/* Server Side (API) */
app.post(API_CREATE_CAR, multer({storage: diskStorage}).single("image_car"), Controller.createCar);
app.get(API_GET_CARS, Controller.getCars);
app.get(API_FILTER_CAR, Controller.filterAPI);
app.get(API_SEARCH_CAR, Controller.searchAPI);
app.get(API_GET_CAR, Controller.getCar);
app.post(API_UPDATE_CAR, multer({storage: diskStorage}).single("image_car"), Controller.updateCar);
app.get(API_DELETE_CAR, Controller.deleteCar);
app.post(API_CREATE_TYPE_CAR, Controller.createTypeCar);
// END API

app.listen(PORT, () => console.log(`Server Running on PORT ${PORT}`));
